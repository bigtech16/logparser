# This code based on https://towardsdatascience.com/getting-started-with-apache-kafka-in-python-604b3250aa05
import sys
import time
import logging
import json

from kafka import KafkaConsumer, KafkaProducer
import redis
import apache_log_parser

logging.basicConfig(stream=sys.stdout, format="%(asctime)s %(levelname)-8s %(message)s", level=logging.INFO)

# Define 2 types of parser
LOGPARSER1 = apache_log_parser.make_parser("%h %l %u %t \"%r\" %s %b \"%{Referer}i\" \"%{User-Agent}i\"")
LOGPARSER2 = apache_log_parser.make_parser("%h %l %u %t \"%r\" %s %b")


def parse_apache_log(logline, topic=None, num=None):
    try:
        result = LOGPARSER1(logline)
    except apache_log_parser.LineDoesntMatchException:
        result = LOGPARSER2(logline)
    except:
        logging.exception("Parsing failed")
        result = None
    result['time'] = result['time_received_tz_isoformat']
    result.pop('time_received_datetimeobj', None)
    result.pop('time_received_isoformat', None)
    result.pop('time_received_tz_datetimeobj', None)
    result.pop('time_received_tz_isoformat', None)
    result.pop('time_received_utc_datetimeobj', None)
    result.pop('time_received_utc_isoformat', None)
    result.pop('request_url_query_dict', None)
    result.pop('request_url_query_list', None)
    result.pop('request_url_query_simple_dict', None)
    return result


def close_kafka(consumer, producer):
    if consumer is not None:
        consumer.close()
    if producer is not None:
        producer.close()


def run_forever(raw_topic_name, parsed_topic_name):
    logging.info('Running Consumer..')
    logging.info('Raw topic: {}'.format(raw_topic_name))
    
    consumer = KafkaConsumer(raw_topic_name, group_id='logparser', bootstrap_servers='localhost:9092')
    producer = KafkaProducer(bootstrap_servers='localhost:9092')
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    r.set(raw_topic_name, 0)
    while True:
        try:
            for msg in consumer:
                logging.info('Message received')
                result = parse_apache_log(msg.value.decode('utf-8'))
                logging.debug(result)
                if result is not None:
                    result['topic'] = raw_topic_name
                    result['num'] = r.incr(raw_topic_name) 
                    key = 'parsed'
                    value = json.dumps(result)
                    bkey = bytes(key, encoding='utf-8')
                    bvalue = bytes(value, encoding='utf-8')
                    producer.send(parsed_topic_name, key=bkey, value=bvalue)
                    producer.flush()
                    logging.info('Message published successfully.')
        except KeyboardInterrupt:
            time.sleep(1)
            break
        except:
            logging.exception("Exception of consumer")
            time.sleep(1)
    close_kafka(consumer, producer)

if __name__ == '__main__':
    import argparse
    ap = argparse.ArgumentParser(prog='logparser.py')
    ap.add_argument('rawtopic', help='Kafka raw data topic name for parsing')
    ap.add_argument('parsedtopic', help='Kafka raw data topic name for parsing')
    args = ap.parse_args()
    # Run forever
    run_forever(args.rawtopic, args.parsedtopic)
