# Log Parser

## Run

```
$ source conda.sh
$ source activate logparser
$ python logparser.py BDTech16th
```

## Kafka command line interface

List topics

```
$ /opt/kafka/current/bin/kafka-topics.sh --list --zookeeper localhost:2181
```

Describe topics

```
$ /opt/kafka/current/bin/kafka-topics.sh --describe --zookeeper localhost:2181 --topic BDTech16th
```

Publish messages

```
$ /opt/kafka/current/bin/kafka-console-producer.sh --broker-list localhost:9092 --topic BDTech16th
```

Subscribe messages

```
$ /opt/kafka/current/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic parsed01 --timeout-ms 100000
```

## References
- https://towardsdatascience.com/kafka-python-explained-in-10-lines-of-code-800e3e07dad1
- https://towardsdatascience.com/getting-started-with-apache-kafka-in-python-604b3250aa05
